import { Component, OnInit } from '@angular/core';
import { OrderService } from '../order/order.service';

@Component({
  selector: 'cg-order-summary',
  templateUrl: './order-summary.component.html'
})
export class OrderSummaryComponent implements OnInit {

  rated: boolean
  protocolo: string

  constructor(private orderService: OrderService) {}

  ngOnInit() {
    this.protocolo = this.orderService.orderId
  }

  rate() {
    this.rated = true
  }

}
