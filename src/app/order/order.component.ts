import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, AbstractControl } from '@angular/forms'

import { Router } from '@angular/router'

import { RadioOption } from '../shared/radio/radio-option.model'
import { OrderService } from './order.service'
import { Order } from "./order.model"
import { LoginService } from '../security/login/login.service';
import { User } from '../security/user.model'

import { tap, finalize } from 'rxjs/operators'

@Component({
  selector: 'cg-order',
  templateUrl: './order.component.html'
})
export class OrderComponent implements OnInit {

  emailPattern = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i

  numberPattern = /^[0-9]*$/

  axisPattern = /^[2-8]$/

  myFormValueChanges$

  orderForm: FormGroup

  order: Order

  constructor(private orderService: OrderService,
    private router: Router,
    private formBuilder: FormBuilder,
    private loginService: LoginService) { }

  ngOnInit() {
    const dateObj = new Date()
    const dateTimeStr = dateObj.toISOString().substr(0, 10).split('-').reverse().join('/') + ' ' + ('0' + dateObj.getHours()).slice(-2) + ':' + ('0' + dateObj.getMinutes()).slice(-2)
    this.orderForm = new FormGroup({
      user: this.formBuilder.control(''),
      destination: new FormGroup({
        name: new FormControl('', {
          validators: [Validators.required, Validators.minLength(5)]
        }),
        address: this.formBuilder.control('', [Validators.required, Validators.minLength(5)]),
        number: this.formBuilder.control('', [Validators.required, Validators.pattern(this.numberPattern)]),
        optionalAddress: this.formBuilder.control(''),
        city: this.formBuilder.control(''),
        zipCode: this.formBuilder.control('', 
        [Validators.required, Validators.minLength(8), Validators.pattern(this.numberPattern)]),
        phone: this.formBuilder.control('', [Validators.minLength(7)]),
        cell: this.formBuilder.control('', [Validators.minLength(7)]),
        email: this.formBuilder.control('', [Validators.pattern(this.emailPattern)])
      }),
      freight: new FormGroup({
        distance: this.formBuilder.control('', [Validators.required, Validators.pattern(this.numberPattern)]),
        cost: this.formBuilder.control(''),
        axis: this.formBuilder.control('', [Validators.required, Validators.pattern(this.axisPattern)]),
        totalCost: this.formBuilder.control({value: '', disabled: true}) // [{value: '', disabled: true}]
      }),
      situations: new FormGroup({
        status: this.formBuilder.control('Solicitado'),
        startDate: this.formBuilder.control(dateTimeStr),
        finishDate: this.formBuilder.control(dateTimeStr),
        observation: this.formBuilder.control('')
      })
    }, { validators: [OrderComponent.equalsTo], updateOn: 'blur' })

    this.myFormValueChanges$ = this.orderForm.controls['freight'].valueChanges;
    this.myFormValueChanges$.subscribe(units => this.updateTotal(units));

  }

  static equalsTo(group: AbstractControl): { [key: string]: boolean } {
    const email = group.get('email')
    const emailConfirmation = group.get('emailConfirmation')
    if (!email || !emailConfirmation) {
      return undefined
    }
    if (email.value !== emailConfirmation.value) {
      return { emailsNotMatch: true }
    }
    return undefined
  }

  private updateTotal(units: any) {
    const control = <FormGroup>this.orderForm.controls['freight']
    const distance = control.get('distance').value
    const axis = control.get('axis').value
    let total = 0
    if (axis != undefined && distance != undefined) {
      control.get('cost').setValue(axis, { onlySelf: true, emitEvent: true })
      total = distance * axis
    }
    control.get('totalCost').setValue(total, { onlySelf: true, emitEvent: false })
  }

  user(): User {
    return this.loginService.user
  }


  isOrderCompleted(): boolean {
    return this.orderService.orderId !== undefined
  }

  checkOrder(order: Order) {
    order.user = this.loginService.user.id
    order.freight.cost = order.freight.axis
    this.orderService.checkOrder(order)
      .pipe(tap((orderId: string) => {
        this.orderService.orderId = orderId
      }))
      .subscribe((orderId: string) => {
        this.router.navigate(['/order-summary'])
      })
    console.log(order)
  }

}
