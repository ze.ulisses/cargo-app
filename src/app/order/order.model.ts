class Order {
  constructor(
    public _id: string,
    public user: string,
    public destination: Destination,
    public freight: Freight,
    public situations: Situation[] = [],
    public id?: string
  ) { }
}

class Destination {
  constructor(
    public name: string,
    public address: string,
    public number: number,
    public optionalAddress: string,
    public city: string,
    public zipCode: string,
    public phone: string,
    public cell: string,
    public email: string
    ) { }
}

class Freight {
  constructor(public distance: number, 
              public axis: number,
              public cost: number) { }
}

class Situation {
  constructor(
    public status: string, 
    public observation: string, 
    public startDate: string,
    public finishDate: string) { }
}

export { Order, Situation }
