import {Injectable} from '@angular/core'

import {HttpClient} from '@angular/common/http'
import {Observable} from 'rxjs'
import {map} from 'rxjs/operators'

import { Order } from './order.model'
import {CARGO_API} from '../app.api'

@Injectable()
export class OrderService {

  orderId: string

  constructor(private http: HttpClient){}


  orderById(id: string): Observable<Order>{
    return this.http.get<Order>(`${CARGO_API}/cargos/${id}`)
  }

  checkOrder(order: Order): Observable<string> {
    return this.http.post<Order>(`${CARGO_API}/cargos`, order)
                    .pipe(map(order => order._id))
  }

}
