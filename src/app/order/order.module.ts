import {NgModule} from "@angular/core"
import {RouterModule, Routes} from '@angular/router'

import {SharedModule} from '../shared/shared.module'

import {OrderComponent} from './order.component'

import {LeaveOrderGuard} from '../order/leave-order.guard'

const ROUTES: Routes = [    
  {path: '', component: OrderComponent, canDeactivate: [LeaveOrderGuard]}
]

@NgModule({
  declarations:[OrderComponent],
  imports: [SharedModule, RouterModule.forChild(ROUTES)]
})
export class OrderModule {}
