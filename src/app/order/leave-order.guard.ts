import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { OrderComponent } from "./order.component";


export class LeaveOrderGuard implements CanDeactivate<OrderComponent>{
    
    canDeactivate(orderComponent: OrderComponent,
                  activateRoute: ActivatedRouteSnapshot,
                  routeState: RouterStateSnapshot): boolean {
                      if(!orderComponent.isOrderCompleted()){
                          return window.confirm('Deseja desistir da solicitação de entrega?')
                      }else{
                          return true
                      }
                  }

}