import {Injectable} from '@angular/core'
import {HttpClient, HttpParams} from '@angular/common/http'

import {Observable} from 'rxjs'

import {Cargo} from "./cargo/cargo.model"
import {LoginService} from "../../app/security/login/login.service"

import {CARGO_API} from '../app.api'

@Injectable()
export class CargosService {

    constructor(private http: HttpClient, private loginService: LoginService){}

    cargos(search?: string): Observable<Cargo[]> {
      let params: HttpParams = undefined
      let user = this.loginService.user === undefined ? undefined : this.loginService.user.id
      if (search){
        params = new HttpParams().append('q', search)
      }
      if (user)
      {
        console.log(`usuario logado: ${user}`)
        return this.http.get<Cargo[]>(`${CARGO_API}/cargos?user=${user}`, {params: params})
      }else{
        return this.http.get<Cargo[]>(`${CARGO_API}/cargos`, {params: params})
      }
    }

    cargoById(id: string): Observable<Cargo>{
      return this.http.get<Cargo>(`${CARGO_API}/cargos/${id}`)
    }

}
