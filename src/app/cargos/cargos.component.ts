import { Component, OnInit } from '@angular/core';
import {trigger, state, style, transition, animate} from '@angular/animations'
import {FormBuilder, FormControl, FormGroup} from '@angular/forms'

import {Router} from '@angular/router'

import {Cargo} from './cargo/cargo.model'
import {CargosService} from './cargos.service'

import {Observable, from} from 'rxjs'
import {switchMap, tap, debounceTime, distinctUntilChanged, catchError, map} from 'rxjs/operators'

@Component({
  selector: 'cg-cargos',
  templateUrl: './cargos.component.html',
  preserveWhitespaces: true,
  animations: [
    trigger('toggleSearch', [
      state('hidden', style({
        opacity: 0,
        "max-height": "0px"
      })),
      state('visible', style({
        opacity: 1,
        "max-height": "70px",
        "margin-top": "20px"
      })),
      transition('* => *', animate('250ms 0s ease-in-out'))
    ])
  ]
})
export class CargosComponent implements OnInit {

  searchBarState = 'hidden'
  cargos: Cargo[]

  searchForm: FormGroup
  searchControl: FormControl

  constructor(private cargosService: CargosService,
              private router: Router,
              private fb: FormBuilder) { }

  ngOnInit() {

    this.searchControl = this.fb.control('')
    this.searchForm = this.fb.group({
      searchControl: this.searchControl
    })

    this.searchControl.valueChanges
        .pipe(
          debounceTime(500),
          distinctUntilChanged(),
          switchMap(searchTerm =>
            this.cargosService
              .cargos(searchTerm)
              .pipe(catchError(error=>from([]))))
        ).subscribe(cargos => this.cargos = cargos.items)

    this.cargosService.cargos()
      //.pipe(tap(cargos => console.log(cargos)),map(cargos => (cargos.map(items => items))))
      .pipe(tap(cargos => console.log(cargos)))
      .subscribe(cargos => this.cargos = cargos
                ,response => this.router.navigate(['/notauthz'])
        )
  }

  toggleSearch(){
    this.searchBarState = this.searchBarState === 'hidden' ? 'visible' : 'hidden'
  }

}
