import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID, ErrorHandler } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, PreloadAllModules } from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms'
import {BrowserAnimationsModule} from '@angular/platform-browser/animations'
import {DatePipe, registerLocaleData} from '@angular/common'
import locatePt from '@angular/common/locales/pt'

registerLocaleData(locatePt, 'pt')

import { ApplicationErrorHandler } from './app.error-handler'
import {ROUTES} from './app.routes'

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { CargosComponent } from './cargos/cargos.component';
import { CargoComponent } from './cargos/cargo/cargo.component';
import { OrderEditComponent } from './order-edit/order-edit.component';
import { OrderSummaryComponent } from './order-summary/order-summary.component'
import { SharedModule } from './shared/shared.module';
import { NotFoundComponent } from './not-found/not-found.component';
import { NotAuthorizedComponent } from './not-authorized/not-authorized.component';
import { LoginComponent } from './security/login/login.component';
import { SignupComponent } from './security/signup/signup.component';
import { UserDetailComponent } from './header/user-detail/user-detail.component';

import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    CargosComponent,
    CargoComponent,
    OrderEditComponent,
    OrderSummaryComponent,
    NotFoundComponent,
    NotAuthorizedComponent,
    LoginComponent,
    SignupComponent,
    UserDetailComponent
   ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    BsDatepickerModule,
    SharedModule.forRoot(),
    RouterModule.forRoot(ROUTES, {preloadingStrategy: PreloadAllModules})
  ],
  providers: [{provide: LOCALE_ID, useValue: 'pt'},
              {provide: ErrorHandler, useClass: ApplicationErrorHandler},
              DatePipe
             ],
  bootstrap: [AppComponent]
})
export class AppModule { }
