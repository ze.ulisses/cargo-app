export interface User {
    id?: string,
    name: string,
    email: string,
    profiles: any[],
    password?: string,
    accessToken?: string
}