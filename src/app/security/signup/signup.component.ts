import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms'
import { SignupService } from './signup.service';
import { NotificationService } from '../../shared/messages/notification.service';
import { ActivatedRoute, Router } from '@angular/router';
import { RadioOption } from 'app/shared/radio/radio-option.model';
import { User } from '../user.model';

@Component({
  selector: 'cg-signup',
  templateUrl: './signup.component.html'
})
export class SignupComponent implements OnInit {

  signupForm: FormGroup
  navigateTo: string

  profileOptions: RadioOption[] = [
    {label: 'Loja', value: 'store'},
    {label: 'Transportadora', value: 'carrier'}
  ]

  constructor(private fb: FormBuilder,
    private signupService: SignupService,
    private notificationService: NotificationService,
    private activatedRoute: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.signupForm = new FormGroup({
      name: this.fb.control('', [Validators.required]),
      email: this.fb.control('', [Validators.required, Validators.email]),
      emailConfirmation: this.fb.control('', [Validators.required, Validators.email]),
      password: this.fb.control('', [Validators.required]),
      passwordConfirmation: this.fb.control('', [Validators.required]),
      profiles: this.fb.control('', [Validators.required])
    }, {validators: [SignupComponent.emailEqualsTo, SignupComponent.passwordEqualsTo]}
    )
    this.navigateTo = this.activatedRoute.snapshot.params['to'] || btoa('/')
  }

  static emailEqualsTo(group: AbstractControl): {[key:string]: boolean} {
    const email = group.get('email')
    const emailConfirmation = group.get('emailConfirmation')
    if(!email || !emailConfirmation){
      return undefined
    }
    if(email.value !== emailConfirmation.value){
      return {emailsNotMatch:true}
    }
    return undefined
  }

  static passwordEqualsTo(group: AbstractControl): {[key:string]: boolean} {
    const password = group.get('password')
    const passwordConfirmation = group.get('passwordConfirmation')
    if(!password || !passwordConfirmation){
      return undefined
    }
    if(password.value !== passwordConfirmation.value){
      return {passwordNotMatch:true}
    }
    return undefined
  }

  signUp(group: AbstractControl) {
    const newUser: User = { name: group.get('name').value, 
                            email: group.get('email').value,
                            password: group.get('password').value,
                            profiles: [group.get('profiles').value]}
    this.signupService.signup(newUser)
      .subscribe(user =>
        this.notificationService.notify(`Bem vindo, ${user.name}`),
        response =>
          this.notificationService.notify(response.error.message),
          ()=>{
            this.router.navigate([atob(this.navigateTo)])
          })
  }

}
