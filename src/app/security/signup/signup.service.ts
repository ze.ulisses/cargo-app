import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'

import { Observable } from 'rxjs';
import {tap, filter} from 'rxjs/operators'

import { CARGO_API } from 'app/app.api';
import { User } from '../user.model';

import { Router, NavigationEnd } from '@angular/router';

@Injectable()
export class SignupService {

    user: User
    lastUrl: string

    constructor(private http: HttpClient, private router: Router) {
        this.router.events.pipe(filter(e => e instanceof NavigationEnd))
                          .subscribe( (e: NavigationEnd) => this.lastUrl = e.url)
     }

    isLoggedIn(): boolean {
        return this.user !== undefined
    }

    signup(user: User): Observable<User> {
        return this.http.post<User>(`${CARGO_API}/users`, user )
            .pipe(tap(user => this.user = user))
    }

    logout(){
        this.user = undefined
    }

    handleLogin(path: string = this.lastUrl){
        this.router.navigate(['/login', btoa(path)])
    }

}