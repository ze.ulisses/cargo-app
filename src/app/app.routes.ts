import {Routes} from '@angular/router'

import {HomeComponent} from './home/home.component'
import {CargosComponent} from './cargos/cargos.component'
import {NotFoundComponent} from './not-found/not-found.component'
import {NotAuthorizedComponent} from './not-authorized/not-authorized.component'
import {LoginComponent} from './security/login/login.component'
import {LoggedInGuard} from './security/login/loggedin.guard'
import {SignupComponent} from './security/signup/signup.component'
import {OrderEditComponent} from './order-edit/order-edit.component';
import { OrderSummaryComponent } from './order-summary/order-summary.component';

export const ROUTES: Routes = [
  {path: '', component: HomeComponent},
  {path: 'login/:to', component: LoginComponent},
  {path: 'login', component: LoginComponent},
  {path: 'signup', component: SignupComponent},
  {path: 'cargos', component: CargosComponent},
  {path: 'order-summary', component: OrderSummaryComponent},
  {path: 'orders', loadChildren: './order/order.module#OrderModule'},
  {path: 'order-edit/:id', component: OrderEditComponent},
  {path: 'about', loadChildren: './about/about.module#AboutModule'},
  {path: 'notauthz', component: NotAuthorizedComponent},
  {path: '**', component: NotFoundComponent}
]
