import { Injectable } from '@angular/core';

import {HttpClient} from '@angular/common/http'
import {Observable} from 'rxjs'

import {Order} from '../order/order.model'

import {CARGO_API} from '../app.api'
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OrderEditService {

  orderId: string

  constructor(private http: HttpClient) { }

  orderById(id: string): Observable<Order>{
    return this.http.get<Order>(`${CARGO_API}/cargos/${id}`)
  }

  updateOrder(order: Order): Observable<string> {
    return this.http.put<Order>(`${CARGO_API}/cargos/${order._id}`, order)
                    .pipe(map(order => order._id))
  }

}
