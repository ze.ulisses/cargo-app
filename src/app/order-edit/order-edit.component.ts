import { ChangeDetectorRef, Component, OnInit, OnChanges } from '@angular/core';
import { FormArray, FormControl, FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router'

import {Order, Situation} from '../order/order.model'
import {OrderEditService} from './order-edit.service'
import { tap } from 'rxjs/operators';
import { NotificationService } from '../shared/messages/notification.service';
import { Destination } from 'app/cargos/cargo/cargo.model';


@Component({
  selector: 'cg-order-edit',
  templateUrl: './order-edit.component.html'
})
export class OrderEditComponent implements OnInit {

  order: Order;
  isLoanding = false;
  headerName: string;
  headerAddress: string;
  headerContact: string;
  
  listStatus = ['Solicitado',
                'Coletado',
                'Armazenado',
                'Expedido',
                'Entregue',
                'Devolvido',
                'Extraviado',
                'Reenviado'];

  numberPattern = /^[0-9]*$/

  orderEditForm: FormGroup

  constructor(private orderEditService: OrderEditService,
              private notificationService: NotificationService,
              private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              protected cdr: ChangeDetectorRef) { }

  ngOnInit() {
    this.orderEditForm = new FormGroup({
      editSituations: this.formBuilder.array([])
    }, { updateOn: 'blur'})
    this.getOrder()

    //this.cdr.detectChanges();
  }

  getOrder() {
    this.orderEditService.orderById(this.route.snapshot.params['id'])
      .subscribe(order => { this.order = order
                            this.setSituations(this.order.situations)
                            this.setAddress(this.order.destination)
      })
  }

  get editSituations(): FormArray {
    return this.orderEditForm.get('editSituations') as FormArray;
  };

  setSituations(situations: Situation[]){
    const situationFormGroups = situations.map(situation => this.formBuilder.group(situation))
    const situationFormArray = this.formBuilder.array(situationFormGroups)
    this.orderEditForm.setControl('editSituations', situationFormArray)
  }

  setAddress(destinations: Destination){
    for (let i in destinations) {
      this.headerName = destinations[i].name;
      this.headerAddress = destinations[i].address;
      if (destinations[i].number != ''){
        this.headerAddress +=  ', ' + destinations[i].number.toString()
      }
      if (destinations[i].optionalAddress != ''){
        this.headerAddress +=  ' - ' + destinations[i].optionalAddress
      }
      this.headerAddress += ' - ' + destinations[i].zipCode
      if (destinations[i].city != ''){
        this.headerAddress += ' - ' + destinations[i].city
      }
      this.headerContact = '' 
      if (destinations[i].email != ''){
        this.headerContact += 'E-mail: ' + destinations[i].email
      }
      if (destinations[i].cell != ''){
        this.headerContact += ' Tel.: ' + destinations[i].cell
      }
      if (destinations[i].phone != ''){
        this.headerContact += ' / ' + destinations[i].phone
      }
    }
  }

  addSituation() {
    const dateObj = new Date()
    const dateTimeStr = dateObj.toISOString().substr(0, 10).split('-').reverse().join('/') + ' ' + ('0' + dateObj.getHours()).slice(-2) + ':' + ('0' + dateObj.getMinutes()).slice(-2)
    this.editSituations.push(this.formBuilder.group(new Situation('', '', dateTimeStr, dateTimeStr)))
  }

  rebuildForm() {
    /*this.orderEditForm.patchValue({
      user: this.order.destination[0].name
    })*/
    //this.setSituations(this.order.situations)
    console.log('executou rebuildForm')
  }

  updateOrder(order: Order){
    this.order = this.prepareSaveOrder()
    console.log(order)
    console.log(this.order)
    /*order.orderItems = this.cartItems()
      .map((item:CartItem)=>new OrderItem(item.quantity, item.menuItem.id))*/
    this.orderEditService.updateOrder(this.order)
      .pipe(tap((orderId: string) => {
        this.orderEditService.orderId = orderId
      }))
      .subscribe(order =>
        this.notificationService.notify(`Alterações gravadas com sucesso.`),
        response =>
          this.notificationService.notify(response.error.message))
  }


  prepareSaveOrder(): Order {
    const formModel = this.orderEditForm.value
    const editSituationsDeepCopy: Situation[] = formModel.editSituations
                    .map((situation: Situation) => Object.assign({}, situation))
    const saveOrder = this.order
    saveOrder.situations = editSituationsDeepCopy
    return saveOrder
  }
}